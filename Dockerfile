FROM node:latest

RUN mkdir -p /usr/src/app
WORKDIR /Users/mmt8728/personal/app

COPY package.json /Users/mmt8728/personal/app

RUN npm config set registry http://registry.npmjs.org/
RUN npm install

COPY . .

EXPOSE 5001

CMD ["node", "app.js"]