#Prerequisite 

- Nodejs
- MongoDb
- Docker

# Getting Started 

Pure Nodejs app. Clone project and run "nodemon app.js"

# Rest APIS

- /create-hosting-data: To create the DB with sample hostname data
- /get-activeip-hostnames/:activeCount : To retrieve hostnames having less or equals activeCount active IP addresses exists. 

