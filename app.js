const express = require("express");
let app = express();
const mongoose = require("mongoose");
const { mockData } = require("./mock");
const { getMtaHostingTable, getHostingActiveStats } = require("./mongoSchema");
const { HostingSchemaName, MappingSchemaName } = require("./config");
const { getHashMapOfHostNameAndIP } = require("./utils");

mongoose.connect("mongodb://mongo:27017/mtaHosting", {
// mongoose.connect("mongodb://localhost:27017/mtaHosting", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.get("/", (req, res) => res.send("Welcome to MTA-Hosting-Optimizer"));

app.get("/create-hosting-data", (request, response) => {
  const ipHosting = getMtaHostingTable(HostingSchemaName);
  ipHosting.collection.insertMany(mockData, { ordered: false });

  // Creating a index/hash table to maintain active/inactive ip counts
  let optmizeActiveCounts = getHashMapOfHostNameAndIP(mockData);

  const hostingActiveMap = getHostingActiveStats(MappingSchemaName);
  Object.keys(optmizeActiveCounts).map((item) => {
    hostingActiveMap.collection.insertOne({
      hostname: item,
      activeCount: optmizeActiveCounts[item]?.activeCount,
      inActiveCount: optmizeActiveCounts[item]?.inActiveCount,
    });
  });
  response.json(optmizeActiveCounts);
});

app.get("/get-activeip-hostnames/:activeCount", (request, response) => {
  const activeCount = Number(request.params.activeCount);
  const hostingActiveMap = getHostingActiveStats(MappingSchemaName);
  hostingActiveMap
    .find(
      { activeCount: { $lte: Number(activeCount) } },
      { hostname: 1, _id: 0 }
    )
    .then((eachOne) => {
      response.json(eachOne);
    });
});

app.listen(5001, function () {
  console.log("Running Mta-Hosting on Port 5001");
});

module.exports = app;
