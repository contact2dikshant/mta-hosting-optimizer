const mongoose = require("mongoose");

var Schema = mongoose.Schema;

const getMtaHostingTableSchema = new Schema({
  ip: String,
  hostname: String,
  active: Boolean,
});
const getHostingActiveStatsSchema = new Schema({
  hostname: String,
  activeCount: Number,
  inActiveCount: Number,
});

const getMtaHostingTable = (schemaName) => mongoose.model(schemaName, getMtaHostingTableSchema);
const getHostingActiveStats = (schemaName) => mongoose.model(schemaName, getHostingActiveStatsSchema);

module.exports = { getMtaHostingTable, getHostingActiveStats };
