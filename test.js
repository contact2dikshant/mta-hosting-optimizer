let chai = require("chai");
let chaiHttp = require("chai-http");
require("mongoose");
let app = require("./app");
chai.should();
const { getMtaHostingTable, getHostingActiveStats } = require("./mongoSchema");
const { HostingSchemaName, MappingSchemaName } = require("./config");
const { mockData } = require("./mock");
const { getHashMapOfHostNameAndIP } = require("./utils");

chai.use(chaiHttp);
let expect = chai.expect;

describe("Mta-Hosting-Unit-Testing", () => {
  it("it should return a Object with hostnames active/inactive ip counts", (done) => {
    let hostnameIpMapping = getHashMapOfHostNameAndIP(mockData);
    let mockResponse = {
      "mta-prod-1": { activeCount: 1, inActiveCount: 1 },
      "mta-prod-2": { activeCount: 2, inActiveCount: 1 },
      "mta-prod-3": { activeCount: 0, inActiveCount: 1 },
    };
    expect(hostnameIpMapping).to.deep.equal(mockResponse);
    done();
  });
});

describe("Mta-Hosting-Integration-Testing-Create", () => {
  beforeEach((done) => {
    getMtaHostingTable(HostingSchemaName).deleteMany({}, () => {});
    getHostingActiveStats(MappingSchemaName).deleteMany({}, () => {});
    done();
  });

  describe("/GET Mta-Hosting", () => {
    it("it should return 200 with empty object", (done) => {
      chai
        .request(app)
        .get("/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
    it("it should create the hosting schema", (done) => {
      chai
        .request(app)
        .get("/create-hosting-data")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("mta-prod-1");
          res.body["mta-prod-1"].should.have.property("activeCount").eql(1);
          res.body["mta-prod-2"].should.have.property("activeCount").eql(2);
          res.body["mta-prod-3"].should.have.property("activeCount").eql(0);
          done();
        });
    });
  });
});

describe("Mta-Hosting-Integration-Testing-Get", () => {
  describe("/GET Mta-Hosting", () => {
    it("it should return the hostname having active count less than or equal to 1", (done) => {
      let X = 1;
      chai
        .request(app)
        .get("/get-activeip-hostnames/" + X)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.length.should.be.eql(2);
          expect(res.body).to.deep.include.members([
            { hostname: "mta-prod-1" },
          ]);
          expect(res.body).to.deep.include.members([
            { hostname: "mta-prod-3" },
          ]);

          done();
        });
    });
    it("it should return the hostname having active count less than or equal to 2", (done) => {
      let X = 2;
      chai
        .request(app)
        .get("/get-activeip-hostnames/" + X)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.length.should.be.eql(3);
          expect(res.body).to.deep.include.members([
            { hostname: "mta-prod-1" },
          ]);
          expect(res.body).to.deep.include.members([
            { hostname: "mta-prod-3" },
          ]);

          done();
        });
    });
    it("it should return the hostname having active count less than or equal to 0", (done) => {
      let X = 0;
      chai
        .request(app)
        .get("/get-activeip-hostnames/" + X)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.length.should.be.eql(1);
          expect(res.body).to.deep.include.members([
            { hostname: "mta-prod-3" },
          ]);
          expect(res.body).not.to.deep.include.members([
            { hostname: "mta-prod-1" },
          ]);
          done();
        });
    });
  });
});
