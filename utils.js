const getHashMapOfHostNameAndIP = function (mock) {
  return mock.reduce((data, value) => {
    data[value.hostname] = {
      activeCount: value.active
        ? Number(data[value.hostname]?.activeCount || 0) + 1
        : Number(data[value.hostname]?.activeCount || 0),
      inActiveCount: value.active
        ? Number(data[value.hostname]?.inActiveCount || 0)
        : Number(data[value.hostname]?.inActiveCount || 0) + 1,
    };
    return data;
  }, {});
};

module.exports = { getHashMapOfHostNameAndIP };
